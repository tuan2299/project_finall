import { useState, useEffect } from "react";
import axios from "axios";
import { useSelector } from "react-redux";

import { ArtistCard, Error, Loader, SongCard } from "../components";
import { useGetTopChartsQuery } from "../redux/services/shazamCore";

const TopArtists = () => {
  const { data, isFetching, error } = useGetTopChartsQuery();

  if (isFetching) return <Loader title="Loading songs around you." />;

  if (error) return <Error />;
  return (
    <div className="flex flex-col">
      <h2 className="mt-4 mb-10 text-3xl font-bold text-left text-white">
        Top Artists
      </h2>
      <div className="flex flex-wrap justify-center gap-8 sm:justify-start">
        {data?.map((track) => (
          <ArtistCard key={track.key} track={track} />
        ))}
      </div>
    </div>
  );
};

export default TopArtists;
